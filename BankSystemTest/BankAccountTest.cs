using Microsoft.VisualStudio.TestTools.UnitTesting;
using BankSystem;
using System;

namespace BankSystemTest
{
    [TestClass]
    public class BankAccountTest
    {
        [TestMethod]
        public void Debit_WithValueLessThanZero_ShouldThrowArgumentOutOfRange()
        {
            //Arrange
            double BeginBalance = 20.00;
            double DebitAmount = -10;
            BankAccount BA = new BankAccount ("Manny",BeginBalance);

            //Act
            try
            {
                BA.Debit(DebitAmount);
            }
            catch (ArgumentOutOfRangeException e)
            {
                StringAssert.Contains(e.Message,BankAccount.DEBIT_AMOUNT_LESS_THAN_ZERO);
                return;
            }
            Assert.Fail("The  Exception Exception was not thrown");
        }


        [TestMethod]
        public void Debit_WithValueGreaterThanBalance_ShouldThrowArgumentOutOfRange()
        {
            //Arrange
            double BeginBalance = 20.00;
            double DebitAmount = 21;
            BankAccount BA = new BankAccount ("Manny",BeginBalance);

            //Act
            try
            {
                BA.Debit(DebitAmount);
            }
            catch (ArgumentOutOfRangeException e)
            {
                StringAssert.Contains(e.Message,BankAccount.DEBIT_AMOUNT_MORE_THAN_MAIN_BALANCE);
                return;
            }
            Assert.Fail("The  Exception Exception was not thrown");
        }
    }
}
