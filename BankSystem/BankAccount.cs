using System;

namespace BankSystem
{
    public class BankAccount
    {
        public const string DEBIT_AMOUNT_LESS_THAN_ZERO = "Debit value is negative";
        public const string DEBIT_AMOUNT_MORE_THAN_MAIN_BALANCE ="Debit value is more than balance";

        public const string CREDIT_AMOUNT_LESS_THAN_ZERO = "Credit value is negative";
        private readonly string Name;
        private double Balance;

        public BankAccount ()
        :this("NoName",0.0){}

        public BankAccount(string name,double balance)
        {
            Name = name;
            Balance = balance;
        }

        public void Debit(double amount)
        {
            if(amount < 0)
            {
                throw new ArgumentOutOfRangeException(amount.ToString(),amount,DEBIT_AMOUNT_LESS_THAN_ZERO);
            }

            if (amount > Balance)
            {
                throw new ArgumentOutOfRangeException(amount.ToString(),amount,DEBIT_AMOUNT_MORE_THAN_MAIN_BALANCE);
            }
            Balance -= amount;
        }

        public void Credit(double amount)
        {
            if (amount < 0)
            {
                throw new ArgumentOutOfRangeException(amount.ToString(),amount,CREDIT_AMOUNT_LESS_THAN_ZERO);
            }

            Balance += amount;
        }
    
    }
}